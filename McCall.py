# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 16:13:52 2016

@author: David Evans
"""

import numpy as np

def bellmanTmap(beta,w,pi,b,alpha,chi,Vcont,Qcont):
    '''
    Iterates of Bellman Equation in Problem 1
    See Homework 3.pdf for Documentation
    '''
    
    
    Vcont_pi = pi.dot(Vcont)
    
    Un_lazy = b + beta * (0.3*Vcont_pi + 0.7*Qcont  )
    Un_Active = b - chi + beta * (0.5*Vcont_pi + 0.5*Qcont )
    
    Q = np.maximum (Un_lazy,Un_Active)
    
    V_Em = w + beta*(alpha*Qcont + (1-alpha)*Vcont)
    
    V = np.maximum (V_Em,Q)
    
    C = (V == V_Em)
    
    E = (Q == Un_Active)
    
    return V,Q,C,E
    
    
def solveInfiniteHorizonProblem(beta,w,pi,b,alpha,chi,epsilon=1e-8):
    '''
    Solve infinite horizon workers problem in Problem 1
    See Homework 3.pdf for Documentation
    '''
    V = np.zeros(len(w))
    Q = 0
    
    diff = 1.
    while diff > epsilon: 
        V_new,Q_new,C,E = bellmanTmap(beta,w,pi,b,alpha,chi,V,Q)
        diff = np.linalg.norm(V-V_new)
        V = V_new
        Q = Q_new
        
    
    return V,Q,C,E
    
def HazardRate(pi,C,E):
    '''
    Computes the hazard rate of leaving unemployment in Problem 1
    See Homework 3.pdf for Documentation
    '''
    Pr_take = pi.dot(C)
    if E==True: 
        Pr_get = 0.5
    else: 
        Pr_get = 0.3
        
    return (Pr_take*Pr_get)
    
    

        
        
def bellmanTmap_HC(beta,w,pi,b,alpha,p_h,h,Vcont,Qcont):
    '''
    Iterates of Bellman Equation of Problem 2
    See Homework 3.pdf for Documentation
    '''
    
    '''
    will be doing if statements
    will be a probability that at the end of the period you might step down, or step up
    should be different if you are at j=0 or J-1(end)(can't step up, can't step down)
    
    '''
    
    J = len(Qcont)
    S = len(Vcont[1,:])
    
    V = np.zeros((J,S))    
    Q = np.zeros(J)
    C = np.zeros((J,S))
    Vem = np.zeros((J,S))

    
    
    
    for j in range(0,J):
        if j ==0:
            Q[0] = b + beta * pi.dot(Vcont[0,:])
        else:
            Q[j] = b + beta * ((p_h * (pi.dot(Vcont[j-1,:]))) + ((1-p_h)*(pi.dot(Vcont[j,:]))))
        if j < J-1:    
            V_fired = p_h*Qcont[j+1] + (1-p_h)*Qcont[j]
            V_not   = p_h*Vcont[j+1,:] + (1-p_h)*Vcont[j,:]
        else:
            V_fired = p_h*Qcont[j] + (1-p_h)*Qcont[j]
            V_not   = p_h*Vcont[j,:] + (1-p_h)*Vcont[j,:]
        
        
        
        Vem[j,:] = w*h[j] + beta*(alpha*V_fired + (1-alpha)*V_not)
            
            
            
        
        V[j,:] = np.maximum(Vem[j,:],Q[j])
        
        
        for s in range(S):


            if V[j,s] == Vem[j,s]:


                C[j,s] = 1
    
    
    
    
    return V,Q,C
    
    '''152.91249693 162.91579192 174.49018256 186.29617834 198.19895312
210.16832195 222.18967569 234.25697 246.34834 258.47326415
270.61278445 282.77573143 294.95186563 307.13480968 319.33662524
331.54617552 343.75849327 355.96895851 368.04995603 378.67527833]'''
    
    
    

def solveInfiniteHorizonProblem_HC(beta,w,pi,b,alpha,hprob,hgrid,epsilon=1e-8):
    '''
    Solve infinite horizon workers problem of Problem 2
    See Homework 3.pdf for Documentation
    '''
    
    
    V = np.zeros((len(hgrid),len(w)))
    Q = np.zeros(len(hgrid))
    
    
    
    diff = 1.
    while diff > epsilon: 
        V_new,Q_new,C = bellmanTmap_HC(beta,w,pi,b,alpha,hprob,hgrid,V,Q)
        diff = np.linalg.norm(V-V_new)
        V = V_new
        Q = Q_new
        
    
    return V,Q,C

    
    
    
    
    
    
    
    